'''
'''

import os
import sys
import json
import logging
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from data_loaders.wikipedia_retrievers import get_us_specific_words

from flask import Flask, render_template, request
#from flask.ext.socketio import SocketIO, emit

# Ensure the tracking-level directory is on the path. Then use . package notation and __init__ files.
this_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if this_dir not in sys.path:
    sys.path.append(this_dir)

# Basic lagging configuration
logging.basicConfig(format='%(levelname)s:\t%(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# Flask App configuration
app = Flask(__name__)
app.debug = False
app.config['SECRET_KEY'] = 'secret!'
#socketio = SocketIO(app)


#client = Elasticsearch()#hosts=['ec2-54-154-19-238.eu-west-1.compute.amazonaws.com'])
client = Elasticsearch(hosts=['ec2-54-76-148-128.eu-west-1.compute.amazonaws.com'], timeout=1000)

INDEX = 'twitter'
DOC_TYPE = 'geotweets'

# -----------------------------------------------------------------------------

@app.route('/')
def main_page():
    """ normal http request to a serve up the page """  

    # Limit to get a sensible amount of data for testing
    limit = request.args.get('limit', None)
    query_term = request.args.get('q', None)

    #US Slang Terms
    page = 'List_of_American_words_not_widely_used_in_the_United_Kingdom'
    dialects = {'us' : get_us_specific_words(page),
                'uk' : []}

    
    if limit:
        s = Search(using=client, index=INDEX) \
            .filter("limit", value=int(limit))
    elif query_term:
        s = Search(using=client, index=INDEX) \
            .filter("term", category="search") \
            .query("match", text=query_term)
    elif limit and query_term:
        s = Search(using=client, index=INDEX) \
            .filter("limit", value=int(limit)) \
            .filter("term", category="search") \
            .query("match", text=query_term)
    else:
        s = Search(using=client, index=INDEX) \
            .filter("limit", value=100000)
        
        #.query(~Q("match", description="beta"))
    
    s.aggs.bucket('my_agg', 'terms', field='place.country_code', size=0) \
          .bucket("nestedx", 'terms', field='text', size=0)
    
    response = s.execute()

    # Build a list of dicts (one for each countries)
    results = {}
    
    for term in response.aggregations.my_agg.buckets:
        
        cc = term['key']
        
        # Key based on country
        if cc != 'us' and cc != 'gb':
            continue
        
        # Loop the specific terms
        for bucket in term['nestedx']['buckets']:
            word, count = bucket['key'], bucket['doc_count']
     
            if results.has_key(word) == False:
                results[word] = {'x':0, 'y':0}
            
            if cc == 'us':
                axis = 'y'
            if cc == 'gb':
                axis = 'x'
            

            results[word][axis] = count
            results[word]['word'] = word
            results[word]['size'] = 5

    data = []
    for word in results:
        
        #if results[word]['x'] < 5 and results[word]['y'] < 5:
        data.append(results[word])
    
    return render_template('index.html', 
                           data=json.dumps(data))

# -----------------------------------------------------------------------------

if __name__ == '__main__':

    app.run()
