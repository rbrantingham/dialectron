
curl -XGET 'http://ec2-54-154-19-238.eu-west-1.compute.amazonaws.com:9200/twitter/tweets/_termvector' -d '{"text" : "important bob dylan"}'

curl -s -XPUT 'http://ec2-54-154-19-238.eu-west-1.compute.amazonaws.com:9200/twitter/' -d '{
  "mappings": {
    "tweets": {
      "properties": {
        "text": {
          "type": "string",
          "term_vector": "with_positions_offsets_payloads",
          "store" : true,
          "index_analyzer" : "fulltext_analyzer"
         }
      }
    }
  },
  "settings" : {
    "index" : {
      "number_of_shards" : 1,
      "number_of_replicas" : 0
    },
    "analysis": {
      "analyzer": {
        "fulltext_analyzer": {
          "type": "custom",
          "tokenizer": "whitespace",
          "filter": [
            "lowercase",
            "type_as_payload"
          ]
        }
      }
    }
  }
}'
    
curl -XGET 'http://localhost:9200/twitter/tweets/_termvector?pretty=true' -d '{
  "doc" : {
    "text" : "to"
  }
}'

curl -XGET 'http://localhost:9200/twitter/tweets/_termvector?pretty=true' -d '{
  "fields" : ["text"],
  "offsets" : true,
  "positions" : true,
  "term_statistics" : true,
  "field_statistics" : true
}'

curl -XGET 'http://localhost:9200/twitter/tweets/_search?search_type=count&pretty=true' -d '{
  "aggregations": {
    "my_agg": {
      "terms": {
        "field": "text"
      }
    }
  }
}
'

curl -XGET 'http://localhost:9200/twitter/tweets/_search?search_type=count&pretty=true' -d '{
  "aggregations": {
    "my_agg": {
      "terms": {
        "field": "lang",
        "size" : 0
      }
    }
  }
}
'

curl -XGET 'http://localhost:9200/twitter/tweets/_search?pretty=true' -d '{
  "aggregations": {
    "my_agg": {
      "terms": {
        "field": "lang",
        "size" : 0
      }
    }
  }
}
'

curl -XGET 'http://localhost:9200/twitter/tweets/_search?search_type=count&pretty=true' -d '{
  "aggregations": {
    "my_agg": {
      "terms": {
        "field": "text",
        "size" : 0
      }
    }
  }
}
'