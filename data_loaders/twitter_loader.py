'''
'''
import re
from twitter import OAuth, TwitterStream, Twitter
from pymongo import MongoClient
from pymongo import GEO2D, ASCENDING, DESCENDING
import geohash
from elasticsearch import Elasticsearch

#---------------------------------------------------------------

def geohash_tweet(tweet):
    ''' Geohash the tweet geo content '''
    
    if tweet.has_key('coordinates') == False:
        return None
        
    #print tweet['coordinates']
    if tweet['coordinates'] == None:
        return None
     
    coords = tweet['coordinates']['coordinates']
    tweet['gh'] = geohash.encode(coords[1], coords[0])

    return tweet

#---------------------------------------------------------------

def build_indexes(coll_handle):
    ''' Builds the relevant indices '''
    
    try:
        coll_handle.create_index([("gh",        ASCENDING)],
                                 name = 'geohash_string')
    except Exception, e:
        print e
        
    try:
        coll_handle.create_index([("gh",        ASCENDING),
                                  ("user.id",   ASCENDING)],
                                 name = 'geohash_userid_string')
    except Exception, e:
        print e

    try:
        coll_handle.create_index([("coordinates.coordinates", GEO2D)],
                                 name = 'geo_only')
    except Exception, e:
        print e

#---------------------------------------------------------------

if __name__ == '__main__':
        
    #/////////// GENERIC /////////////////
    
    # names for both mongo and es. the doc type for es gets overwritten for non-geos
    db_index = 'twitter'
    collection_type = 'geotweets'
    ES_INDEX = db_index
    ES_TYPE = collection_type
    
    #/////////// MONGO /////////////////
    
    mongo_insert = False
    #Conn to mongo
    client = MongoClient()
    db = client[db_index]
    #Geo collection
    coll = db[collection_type]
    # Create indexes
    build_indexes(coll)
    
    #/////////// ELASTICSEARCH /////////////////
    
    es_insert = True
    es = Elasticsearch(hosts=['ec2-54-154-19-238.eu-west-1.compute.amazonaws.com'])
    
    #/////////// TWITTER /////////////////
    
    oAuthToken = '306927477-RsIn3id9AiZAKyZCB6ddL0Qa9nfLxt9XYXeAIkIO'
    oAuthSecret = 'yetUC1BFJGSLPPj55q1YfjA2awVVTowb1KY4EVPIObKY9'
    oAuthConsumerKey = 'Bbc8DsSYdq0ugrX7X9y6V58Vu'
    oAuthConsumerSecret = 'BfKUvjNQAaq0voCpYvoRXJkYdKyST7dAfCVPMNvyu2RgQjTqFT'

    # Retweet regex
    regExp = re.compile(r'\s{0,2}rt | rt\s{0,2}', re.IGNORECASE)

    #Get the twitter steam data
    t = TwitterStream(auth=OAuth(oAuthToken, oAuthSecret, oAuthConsumerKey, oAuthConsumerSecret))
    samp = t.statuses.sample()
    
    #/////////// COUNTERS /////////////////
    
    #This will keep going forever
    totcnt = 0
    geo_count = 0
    nogeo = 0
    nokey = 0
    sav = []
    
    max_geo_tweets = 1000000
    
    #Works - just storing those with geo
    for tweet in samp:
        
        #print tweet
        
        # Ignore some tweet types        
        if tweet == None or tweet.has_key('delete') == True:
            continue

        if tweet.has_key('text') == False:
            continue

        # Only take tweets where either the user or tweet is a type of english
        if tweet['lang'].lower().startswith('en') == False:
            continue
        
        #tweet['user']['lang'].lower().startswith('en') == False:
        #    continue

        if re.match(regExp, tweet['text']) != None:
            continue

        # ignore the object id
        try:
            _id = tweet.pop('_id')
        except KeyError:
            pass
        
        try:
            if tweet.has_key('geo')==True and tweet['geo']==None:
                nogeo+=1
    
                ES_TYPE  = 'tweets'
    
                # Fire into Elasticsearch
                if es_insert == True:
                    res = es.index(index=ES_INDEX, doc_type=ES_TYPE, body=tweet)

                    
            elif tweet.has_key('geo')==True and tweet['geo']!= None:
                
                ES_TYPE  = 'geotweets'
                
                # Geohash the tweets coordinates
                tweet = geohash_tweet(tweet)
                if not tweet:
                    continue
                                
                # Insert the tweet
                if mongo_insert == True:
                    pid = coll.insert(tweet)
                
                # Fire into Elasticsearch
                if es_insert == True:
                    try:
                        _id = tweet.pop('_id')
                    except:
                        pass
                    es.index(index=ES_INDEX, doc_type=ES_TYPE, body=tweet)
                
                geo_count+=1
                
                if geo_count % 1000 == 0:
                    print 'inserted tweets: %s' %(geo_count)
                
                if geo_count >= max_geo_tweets:
                    print 'breaking'
                    break

        except Exception, e:
            print e
            nokey+=1
            
        totcnt+=1
    del t
    del samp
    print 'geo:{geo}, nogeo:{nogeo}, nokey:{nokey}, tot:{totcnt}'.format(geo=geo_count, nogeo=nogeo, nokey=nokey, totcnt=totcnt)
    print 'done'
    




