import os
import sys

import json

import requests

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from data_loaders.wikipedia_retrievers import get_us_specific_words

client = Elasticsearch(hosts=['ec2-54-76-148-128.eu-west-1.compute.amazonaws.com'], timeout=1000)

INDEX = 'twitter'
DOC_TYPE = 'geotweets'

#US Slang Terms
page = 'List_of_American_words_not_widely_used_in_the_United_Kingdom'
dialects = {'us' : {'slang_terms': get_us_specific_words(page)},
            'gb' : {'slang_terms': []}}

limit = 1000

# Aggregation just by country code
s = Search(using=client, index=INDEX) \
    .filter("limit", value=limit)
s.aggs.bucket("cc_agg", 'terms', field='place.country_code', size=0)

response = s.execute()
for result in response.aggregations.cc_agg.buckets:
    
    cc = result['key'].lower()
    count = int(result['doc_count'])
    try:
        dialects[cc]['total_tweet_count'] = count        
    except:
        pass

# Aggregation by term/country code
s = Search(using=client, index=INDEX) \
    .filter("limit", value=limit)
    #.filter("term", category="search") \
    #.query("match", title="python")   \
    #.query(~Q("match", description="beta"))

s.aggs.bucket('my_agg', 'terms', field='text', size=0) \
      .bucket("nestedx", 'terms', field='place.country_code', size=0)

# This for output as shown below
#s.aggs.bucket('my_agg', 'terms', field='text', size=0) \
#      .bucket("nestedx", 'terms', field='place.country_code', size=0)
      #.metric('max_lines', 'max', field='lines')

"""
{
  "vernaculars": [
    "us"
  ], 
  "nestedx": {
    "buckets": [
      {
        "key": "my", 
        "doc_count": 1
      }
    ], 
    "sum_other_doc_count": 0, 
    "doc_count_error_upper_bound": 0
  }, 
  "key": "johnson", 
  "doc_count": 3
}
"""


response = s.execute()

# Build a list of dicts (one for each countries)
results = {}

for term in response.aggregations.my_agg.buckets:
    cc = term['key']
    print term
    # Only take those we're interested in - see top
    if cc not in dialects.keys():
        continue
    
    # Loop the specific terms
    for bucket in term['nestedx']['buckets']:
        print bucket
        word, count = bucket['key'], bucket['doc_count']
 
        if results.has_key(word) == False:
            results[word] = {}
        
        if cc == 'us':
            axis = 'y'
        if cc == 'gb':
            axis = 'x'
            
        results[word][axis] = count
        results[word]['word'] = word
        results[word]['size'] = 5
        

output_dict = {}
for key in dialects.keys() + ['unknown']:
    output_dict[key] = []

us_words = dialects['us']
gb_words = dialects['gb']

#print us_words
#print gb_words

for word in results:
    print word, results[word]
    
    #if word in us_words:



"""
[{"keys" : "US",
  "values" : [{"word" : xxx,
               "size" : 10,
               "x" : x-pos,
               "y" : y-pos}
              ]},
 {"keys" : "UK",
  "values" : [
              ]},
 {"keys" : "Unknown",
  "values" : [
              ]},
"""

"""
for term in response.aggregations.my_agg.buckets:
    #if term['key'] == 'windshield':
    #    print json.dumps(term, indent=2)
    #print term['key'], term['doc_count']

    # Cross reference the terms in all the tweets against the slang terms
    # Add a field which indicates which nations the words is used in
    for key in local_terms.keys():
        if term['key'] in local_terms[key]:
            try:
                term['vernaculars'].append(key)
            except:
                term['vernaculars'] = [key]

    #if term.has_key('vernaculars') == True and len(term['nestedx']['buckets']) > 0:
    print json.dumps(term, indent=2)
"""

"""
curl -XGET 'http://localhost:9200/twitter/tweets/_search?search_type=count&pretty=true' -d '{
  "aggregations": {
    "my_agg": {
      "terms": {
        "field": "text",
        "size" : 0
      }
    }
  }
}
'
"""