import wikipedia
import string
#page = 'List_of_American_words_not_widely_used_in_the_United_Kingdom'
page = 'List_of_British_words_not_widely_used_in_the_United_States'
#print wiki_page.content
#print wiki_page.html()

def matches_section_letter(section_letter, this_word):
    """ Checks that the word starts with the right letter to be in this section """
    
    if this_word[0] == section_letter.lower():
        return 1
    
    
def check_order(section_letter, last, this):
    """ Checks that the word we think we've pulled is in the sequence
        Crude check that we're pulling the correct word and not the description
        
        NOT IN USE YET
        
        """
    
    match = 1
    
    # Get the first letters of both words and check they're in alphabetical order
    last_letter_0 = last[0].lower()
    this_letter_0 = this[0].lower()
        
    if this_letter_0 != last_letter_0:
        match = 0
    
    try:
        last_letter_1 = last[0]
    except:
        last_letter_1 = None
    try:
        last_letter_1 = last[0]
    except:
        this_letter_1 = None
    

def get_us_specific_words(page):
            
    wiki_page = wikipedia.page(page)

    all_the_letters = string.uppercase
    
    terms_list = []
    #print wiki_page.content
    
    for letter in all_the_letters:
        section_content = wiki_page.section(letter)
        
        # Ignore empty sections    
        if not section_content:
            continue
    
        # Loop the lines in the section
        lines = section_content.split('\n')        
        i = 0
        last_correct_word = 0
        for line in lines:
            i+=1
            
            # Check whether this line could be a genuine word rather than explanation
            if not matches_section_letter(letter, line):
                continue
    
            # 2 gen words shouldnt follow each other
            if i == last_correct_word + 1:
                continue
    
            # Cleanup
            line = line.split('(')[0]
            line = line.replace('*','')
            line_list = line.split(',')
            
            last_correct_word = i
            
            for term in line_list:
                terms_list.append(term.strip())
    
    return terms_list 
                
            
            
#x = get_country_specific_words(page)         
#for ii in x:
#    print ii
            
            
# GET GOOD SLANG WORD LISTS TIED TO COUNTRIES
# 
            
            